Source: tt-rss
Section: web
Priority: optional
Maintainer: Sebastian Reichel <sre@debian.org>
Uploaders: Marcelo Jorge Vieira (metal) <metal@debian.org>,
           Sunil Mohan Adapa <sunil@medhas.org>
Build-Depends: debhelper-compat (= 13),
               libjs-prototype (>= 1.7.1-3.1)
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: http://tt-rss.org
Vcs-Git: https://salsa.debian.org/debian/tt-rss.git
Vcs-Browser: https://salsa.debian.org/debian/tt-rss

Package: tt-rss
Built-Using: prototypejs (= 1.7.1-3.1)
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         dbconfig-common,
         dbconfig-mysql | dbconfig-pgsql |  dbconfig-no-thanks,
         libjs-dojo-core (>= 1.9.0),
         libjs-dojo-dijit (>= 1.9.0),
         libjs-scriptaculous,
         lsb-base (>= 3.0-6),
         php-php-gettext (>= 1.0.11-4),
         libapache2-mod-php (>= 5.6.0) | php-cgi (>= 5.6.0) | php (>= 5.6.0),
         fonts-material-design-icons-iconfont,
         php-cli,
         php-intl,
         php-json,
         php-mbstring,
         php-mysql | php-pgsql,
         php-psr-log,
         php-xml,
         phpqrcode
Recommends: apache2 | lighttpd | httpd,
            php-curl,
            php-gd,
            php-mcrypt,
            ca-certificates
Suggests: sphinxsearch,
          php-apc
Description: Tiny Tiny RSS - web-based news feed (RSS/Atom) aggregator
 Tiny Tiny RSS is designed to allow you to read news from any location, while
 feeling as close to a real desktop application as possible.
 .
 Feature list:
  * server-side application - user only needs a web browser;
  * support for RSS, RDF, Atom feeds;
  * streamlined interface using AJAX;
  * authentication for reading protected feeds;
  * OPML import/export;
  * feed aggregation;
  * keyboard shortcuts;
  * content filtering;
  * JSON-based RPC API.
